CARLA_BUILD_FOLDER=$(CURDIR)/Build
CARLA_BUILD_CONTENT_FOLDER=$(CARLA_BUILD_FOLDER)/Unreal/CarlaUE4/Content/Carla

PACKAGE_FILE=$(shell git log --pretty=format:'%cd_%h' --date=format:'%Y%m%d' -n 1 | cut -c 1-16).tar.gz


define log
  echo "\033[1;35m$1\033[0m"
endef

default: dist

dist: $(PACKAGE_FILE)
	@$(call log,Package generated at $(PACKAGE_FILE).)

upload-dist: dist
	@aws s3 cp $(PACKAGE_FILE) s3://carla-assets/$(PACKAGE_FILE) --profile Jenkins-CVC --endpoint-url=https://s3.us-east-005.backblazeb2.com/
	@$(call log,Package uploaded to s3://carla-assets/$(PACKAGE_FILE).)

%.tar.gz:
	@git archive -v --format tar.gz HEAD -o $@

check:
	@echo "Looking for Non-ASCII characters in file names"
	@LC_ALL=C find . -name '*[! -~]*' > non-ascii.txt
	@cat non-ascii.txt
	@test ! -s non-ascii.txt
	@rm -f non-ascii.txt
	@echo "OK"

release: dist
	@$(call log,Downloading CARLA (master)...)
	@if [ ! -d $(CARLA_BUILD_FOLDER) ]; then git clone https://github.com/carla-simulator/carla $(CARLA_BUILD_FOLDER); fi
	@$(call log,Extracting Content...)
	@rm -Rf $(CARLA_BUILD_CONTENT_FOLDER)
	@mkdir -p $(CARLA_BUILD_CONTENT_FOLDER)
	@tar -xvzf $(PACKAGE_FILE) -C $(CARLA_BUILD_CONTENT_FOLDER) >/dev/null
	@$(call log,Building CARLA Release...)
	@cd $(CARLA_BUILD_FOLDER); rm -Rf ./Dist; git checkout .; git pull
	@cd $(CARLA_BUILD_FOLDER); make clean package
	@$(call log,Release package generated at $(CARLA_BUILD_FOLDER)/Dist)
