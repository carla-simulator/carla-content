#!/usr/bin/env groovy

pipeline
{
    agent none

    environment
    {
        UE4_ROOT = '/home/jenkins/UnrealEngine_4.26'
    }

    options
    {
        buildDiscarder(logRotator(numToKeepStr: '2', artifactNumToKeepStr: '2'))
    }

    stages
    {
        stage('Building')
        {
            agent { label "assets_agent" }
            stages
            {
                stage('Check Filenames')
                {
                    steps
                    {
                        sh 'make check'
                    }
                }

                stage('Package Assets') 
                {
                    steps 
                    {
                        sh 'rm -f *.tar.gz'
                        sh 'make dist'
                    }
                    post 
                    {
                        success 
                        {
                            archiveArtifacts '*.tar.gz'
                        }
                    }
                }
                stage('Upload Assets') 
                {
                    steps 
                    {
                        sh 'make upload-dist'
                    }
                }
            }

            post
            {
                always {
                    always
                    {
                        cleanWs { // Clean after build
                            cleanWhenAborted(true)
                            cleanWhenFailure(false)
                            cleanWhenNotBuilt(false)
                            cleanWhenSuccess(true)
                            cleanWhenUnstable(true)
                            deleteDirs(true)
                            notFailBuild(true)
                            disableDeferredWipeout(true)
                        }
                    }
                }
            }
        }
    }
}
